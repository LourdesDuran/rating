const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const session = require('express-session')
const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');
const app = express()


app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.json())
app.use(express.static('public'))



//Conecta la base de datos
mongoose.connect("mongodb+srv://lourdesdurandt:12345678adminduran@cluster0.jpqigxv.mongodb.net/?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});



app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.json())
app.engine('handlebars', exphbs.engine({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');
app.use(methodOverride('_method'))
app.use(express.static(__dirname + '/public'))



//controller
const local_auth = require('./controllers/local_auth')(app)
const reviews = require("./controllers/reviews")(app);
const comments = require('./controllers/comments')(app);


app.set('port', process.env.PORT || 3000)
app.listen(app.get('port'), () => {
  console.log('Server on port', app.get('port'));
})