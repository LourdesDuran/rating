
const Review = require('../models/review');
const Comment = require('../models/comment')
const User = require('../models/user')
const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt');


const app = require('../controllers/local_auth')

module.exports = function(app){

  const session= require('express-session')
app.use(passport.initialize());
app.use(passport.session());

//función middleware para verificar si un usuario está autenticado antes de permitir que acceda a ciertas rutas.
function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) return next();
	res.redirect('/login');
}
//pagina login
app.get('/login', (req, res) =>{
  res.render('login', {title:"login"})
})

//Pagina principal home
app.get('/',isLoggedIn, (req, res)=>{
  Review.find().lean()
  .then(reviews =>{
      res.render("reviews-index",{username:req.user.username, reviews:reviews});
  })
  .catch(err =>{
      console.log(err)
  })
});


// ruta para autenticar a un usuario mediante Passport.
app.post('/login', passport.authenticate('local', {
	successRedirect: '/',
	failureRedirect: '/login?error=true'
}));

// ruta para cerrar la sesión de un usuario.
app.get('/logout', function(req, res){
    req.logout(function(err) {
      if (err) { return next(err); }
      res.redirect('/');
    });
  });


//cuando un usuario navega a la URL '/signup', el servidor responde con una página que muestra un formulario de registro.
app.get('/signup', function(req, res, next) {
    res.render('signup');
  });



//ruta para registrar un nuevo usuario en la base de datos.
  app.post('/signup', async (req, res, next) => { 
    const { username, password } = req.body;
    // Check if user already exists
    const existingUser = await User.findOne({ username });
    if (existingUser) {
      return res.status(409).send('User existiert schon');
    }
    // Hash the password using bcrypt
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    // Create a new user with the hashed password
    const newUser = new User({
      username,
      password: hashedPassword,
    });
    // Save the new user to the database
    try {
      await newUser.save();
      req.login(newUser, (err) => {
        if (err) return next(err);
        return res.redirect('/');
      });
    } catch (err) {
      next(err);
    }
  });
  


//ruta para mostrar un formulario para que los usuarios puedan agregar una nueva reseña.
    app.get('/reviews/new', (req, res) => {
        res.render('reviews-new', {});
      })
    
// ruta para agregar una nueva reseña a la base de datos
      app.post('/reviews', (req, res) => {
      //req. body contiene los datos  
        console.log(req.body);
        req.body.username = req.user.username
            console.log(req.body.username)
    
        Review.create(req.body).then ((review) =>{
            console.log(review);
            res.redirect('/');
        }).catch((err) => {
            console.log (err.message);
        })
      })

//ruta para mostrar una reseña específica y sus comentarios. 
      app.get('/reviews/:id', (req, res) => {
        // find review
        Review.findById(req.params.id).lean().then(review => {
          // fetch its comments
          Comment.find({ reviewId: req.params.id }).lean().then(comments => {
            console.log( 'controllers/reviews.js, line 127: Neues Comment aus der DB:');
            console.log( comments );
            

            // respond with the template with both values
            res.render('reviews-show', {  review: review, comments: comments });
          })
        }).catch((err) => {
          // catch errors
          console.log(err.message)
        });
      });
    
//ruta para mostrar un formulario para que los usuarios puedan modificar una reseña existente.
    app.get('/reviews/:id/edit', (req, res) => {
      Review.findById(req.params.id).lean().then((review) => {
        if(req.isAuthenticated() && req.user.username === review.username){
      //Render The edit page
      res.render('reviews-edit', {review:review});
    } else{
      res.render('error', { message: 'Sie haben keine berachtigung zu andern'})
    }

      }).catch((error) => {
        console.log(err.message)
      })
    })
    








    
// UPDATE  ruta para actualizar una reseña existente en la base de datos.
    app.put('/reviews/:id', async(req, res, next) => {
      try{
        const review = await Review.findByIdAndUpdate(req.params.id, req.body, { new: true}).lean();
        console.log();
        res.render('reviews-show', {review: review})
      } catch(err) {
          next(err);
        }
        })

    


    
    
}
