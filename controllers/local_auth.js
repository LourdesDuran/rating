const passport = require('passport')
const bcrypt = require('bcrypt')
const LocalStrategy = require('passport-local').Strategy
const session = require('express-session')
const User = require('../models/user')
const express = require('express')
const bodyParser = require('body-parser')

module.exports = (app) => {

app.use(session({
    secret: "verygoodsecret",
    resave:false,
    saveUninitialized:true
  
  }))

  app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.json())
  
  app.use(passport.initialize());
  app.use(passport.session())


  //toma el usuario y lo serializa a un identificador único, en este caso el id del usuario, que se puede almacenar en la sesión del usuario. 
  passport.serializeUser(function (user,done){
    done(null, user.id);
  });
  
  //toma el identificador único serializado y lo deserializa para obtener el objeto de usuario correspondiente.
  passport.deserializeUser(function(id,done){
    User.findById(id).then(user => {
        done(null, user);
      }).catch(err => {
        done(err, null);
      });
  
  });

//Estrategia de autenticación que comprueba si el nombre de usuario y la contraseña proporcionados son válidos.
passport.use(new LocalStrategy(async function (username, password, done) {

    //Busca en Mongoose un usuario con ese nombre, Si no se encuentra ningún usuario, devuelve un mensaje de error.
      try {
        const user = await User.findOne({ username: username });
        if (!user) {
          return done(null, false, { message: "Incorrect username." });
        }
    
    //Comparar la contraseña proporcionada con la contraseña almacenada en la base de datos para ese usuario.
        const match = await bcrypt.compare(password, user.password);
        if (!match) {
          return done(null, false, { message: "Incorrect password." });
        }
        console.log('User:', user);
        return done(null, user);
      } catch (error) {
        return done(error);
      }
    }))


    //Crea un usuario administrador en una aplicación web 
app.get('/setup', async (req, res) => {
	const exists = await User.exists({ username: "admin" });
	if (exists) {
		res.redirect('/login');
		return;
	};

//comprueba si existe un usuario "admin" en MongoDB utilizando el método "exists" de Mongoose.
//Si existe el usuario "admin", se redirige al usuario a la página de inicio de sesión con la función "res.redirect('/login')" y se sale de la ruta con "return".
	

bcrypt.genSalt(10, function (err, salt) {
		if (err) return next(err);
		bcrypt.hash("pass", salt, function (err, hash) {
			if (err) return next(err);		
			const newAdmin = new User({
				username: "admin",
				password: hash
			});
			newAdmin.save();
			res.redirect('/login');
		});
	});
});
}