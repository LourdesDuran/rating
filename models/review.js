const mongoose = require('mongoose');
const Comment = require('../models/comment')

const Review = mongoose.model('Review', {
    title: String,
    author: String,
    username: String,
    description: String,
    bookTitle: String,
    rating: Number,
    keywords: String
  });
  module.exports = Review;